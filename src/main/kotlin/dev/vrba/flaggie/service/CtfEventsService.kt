package dev.vrba.flaggie.service

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import dev.vrba.flaggie.discord.DiscordConfiguration
import dev.vrba.flaggie.entity.CtfEvent
import dev.vrba.flaggie.repository.CtfEventsRepository
import dev.vrba.flaggie.shared.CzechDateTimeFormatter
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.JDA
import org.springframework.stereotype.Service
import java.time.LocalDateTime

@Service
class CtfEventsService(private val repository: CtfEventsRepository, client: JDA, config: DiscordConfiguration) {

    private val eventsChannel = client.getTextChannelById(config.eventsChannel)
            ?: throw IllegalStateException("Cannot find events channel")

    data class RawEventData(
            val name: String,
            val website: String,
            val start: String,
            val end: String,
            val format: String? = null,
            val access: String? = null
    )

    data class ValidationError(val reason: String)

    fun create(data: RawEventData): Either<ValidationError, CtfEvent> =
            validate(data)
                    .map {
                        val message = eventsChannel.sendMessageEmbeds(
                                EmbedBuilder()
                                        .setTitle(it.name, it.website)
                                        .addField("Duration", "${it.start} - ${it.end}", false)
                                        .addField("Format", it.format ?: "-", false)
                                        .addField("Access", "||${it.access ?: "-"}||", false)
                                        .setColor(0xEB459E)
                                        .build())
                                .complete()

                        message.addReaction("✅").queue()

                        val role = eventsChannel.guild.createRole()
                                .setName(it.name)
                                .setColor(0xFEE75C)
                                .setMentionable(true)
                                .complete()

                        repository.save(CtfEvent(
                                name = it.name,
                                url = it.website, // TODO: Rename this to url?
                                start = parse(it.start),
                                end = parse(it.end),
                                access = it.access,
                                format = it.format,
                                messageId = message.id,
                                roleId = role.id
                        ))
                    }

    private fun validate(data: RawEventData): Either<ValidationError, RawEventData> {
        data.name.isNotBlank() || return ValidationError("Name must not be blank").left()
        data.website.isNotBlank() || return ValidationError("Website must not be blank").left()
        safelyParse(data.start) ?: return ValidationError("Invalid start date").left()
        safelyParse(data.end) ?: return ValidationError("Invalid end date").left()

        val start = parse(data.start)
        val end = parse(data.end)

        start.isBefore(end) || return ValidationError("Start date must be before end date").left()
        start.isAfter(LocalDateTime.now()) || return ValidationError("Start date must be after today").left()

        return data.right()
    }

    private fun safelyParse(date: String): LocalDateTime? =
            try {
                parse(date)
            } catch (exception: Exception) {
                null
            }

    private fun parse(date: String): LocalDateTime = LocalDateTime.parse(date, CzechDateTimeFormatter.default)
}