package dev.vrba.flaggie.entity

import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "ctf_events")
data class CtfEvent(
    @Id
    val id: UUID = UUID.randomUUID(),

    val name: String,

    val url: String,

    val start: LocalDateTime,

    val end: LocalDateTime,

    // ID of the associated Discord role
    val roleId: String,

    // ID of the associated Discord message (for collecting reactions)
    val messageId: String,

    val format: String? = null,

    // Team code / access key ...
    val access: String? = null,

)