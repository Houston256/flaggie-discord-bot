package dev.vrba.flaggie.discord

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "discord")
class DiscordConfiguration {

    lateinit var token: String

    lateinit var guild: String

    lateinit var eventsChannel: String

    // Add more properties mapped from application.yml
}