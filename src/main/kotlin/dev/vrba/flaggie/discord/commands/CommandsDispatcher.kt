package dev.vrba.flaggie.discord.commands

import dev.vrba.flaggie.discord.DiscordConfiguration
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.springframework.stereotype.Component

@Component
final class CommandsDispatcher(client: JDA, config: DiscordConfiguration, providers: List<CommandsProvider>): ListenerAdapter() {
    private val handlers: Map<String, CommandsProvider> = providers.flatMap {
                provider -> provider.commands.map { it.name to provider }
            }.toMap()

    init {
        val guild = client.getGuildById(config.guild)

        providers.flatMap { it.commands }
                .forEach { guild?.upsertCommand(it) }

        client.addEventListener(this)
    }

    override fun onSlashCommand(event: SlashCommandEvent) {
        if (handlers.containsKey(event.name)) {
            handlers[event.name]?.handle(event)
        }
    }
}