package dev.vrba.flaggie.discord.commands

import dev.vrba.flaggie.service.CtfEventsService
import dev.vrba.flaggie.service.CtfEventsService.RawEventData
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import org.springframework.stereotype.Component

@Component
class EventsCommandProvider(private val service: CtfEventsService) : CommandsProvider {

    override val commands: List<CommandData> = listOf(
            CommandData("event", "Create a new CTF event")
                    .addOption(OptionType.STRING, "name", "Name of the event (eg. Google CTF 2021)", true)
                    .addOption(OptionType.STRING, "website", "Website of the event (eg. https://g.co/ctf)", true)
                    .addOption(OptionType.STRING, "start", "Start of the event (eg. \"24. 12. 2021 18:00\")", true)
                    .addOption(OptionType.STRING, "end", "End of the event (eg. \"25. 12. 2021 17:00\")", true)
                    .addOption(OptionType.STRING, "format", "Format of the event (eg. Jeopardy)", false)
                    .addOption(OptionType.STRING, "access", "Password/Team access token for registration", false)
    )

    override fun handle(event: SlashCommandEvent) {
        service.create(
                RawEventData(
                        requireNotNull(event.getOption("name")).asString,
                        requireNotNull(event.getOption("website")).asString,
                        requireNotNull(event.getOption("start")).asString,
                        requireNotNull(event.getOption("end")).asString,
                        event.getOption("format")?.asString,
                        event.getOption("access")?.asString
                )
        )
                .fold(
                        { error ->
                            event.replyEmbeds(
                                    EmbedBuilder()
                                            .setColor(0xED4245)
                                            .setTitle("Invalid arguments provided")
                                            .setDescription(error.reason)
                                            .build())
                                    .queue()
                        },
                        {
                            // Discord api needs to be called within the time frame of 3 seconds,
                            // As there can be some delay with creating role / channels / message it is delayed
                            event.deferReply()
                                .complete()
                                .sendMessageEmbeds(
                                    EmbedBuilder()
                                            .setColor(0x57F287)
                                            .setTitle("CTF Event created")
                                            .build()
                                )
                                .queue()
                        }
                )
    }
}