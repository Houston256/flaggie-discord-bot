package dev.vrba.flaggie.shared

import java.time.format.DateTimeFormatter

/**
 * https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
 */
object CzechDateTimeFormatter {
    val default: DateTimeFormatter = DateTimeFormatter.ofPattern("d. M. yyyy H:m")
}