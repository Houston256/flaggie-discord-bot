package dev.vrba.flaggie.repository

import dev.vrba.flaggie.entity.CtfEvent
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CtfEventsRepository : CrudRepository<CtfEvent, UUID> {
}